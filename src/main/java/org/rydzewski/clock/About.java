package org.rydzewski.clock;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

@SuppressWarnings("serial")
class About extends JDialog {
	
	public About(JFrame parent) {
		super(parent);
		setBounds(new Rectangle(100, 100));
		setModal(true);
		
		JButton button = new JButton("baton");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		add(button);
		
	}
}

