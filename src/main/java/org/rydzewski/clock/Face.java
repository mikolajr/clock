package org.rydzewski.clock;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Path2D.Double;
import java.util.Calendar;
import java.util.TimeZone;

import javax.swing.JPanel;

@SuppressWarnings("serial")
class Face extends JPanel implements Runnable {

	TimeZone timeZone = TimeZone.getDefault();
	Thread t;
	Stroke normalStroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
	Stroke hoursStroke = new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
	Color red = new Color(255, 0, 0, 50);
	
	public Face() {
		super();
		setDoubleBuffered(true);
		t = new Thread(this);
		t.setDaemon(true);
		t.start();
	}
	
	private Double createFaceShape(int r) {
		Double path = new Path2D.Double();
		
		for(int i=0; i<=12; i++) {
			double angle = 360/12*i - 90;
			
			int x1 = (int)(Math.cos(Math.toRadians(angle)) * (double)r*0.95);
			int y1 = (int)(Math.sin(Math.toRadians(angle)) * (double)r*0.95);
			
			int x2 = (int)(Math.cos(Math.toRadians(angle)) * (double)r);
			int y2 = (int)(Math.sin(Math.toRadians(angle)) * (double)r);
			
			path.moveTo(x1, y1);
			path.lineTo(x2, y2);
		}
		
		return path;
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		Dimension size = getSize();
		int wx = (int)(size.getWidth()/2);
		int wy = (int)(size.getHeight()/2);
		int smallerSide = Math.min(wx, wy);
		int r = wx > wy ? (int)(wy*0.9) : (int)(wx*0.9);

		g2.clearRect(0, 0, (int)size.getWidth(), (int)size.getHeight());

	   AffineTransform at = AffineTransform.getTranslateInstance(getWidth()/2, getHeight()/2);
	   Shape shape = at.createTransformedShape(createFaceShape(smallerSide));
       g2.draw(shape);

		Calendar c= Calendar.getInstance();
		TimeZone tz = getTimeZone();
		if (tz != null)
			c.setTimeZone(tz);
		
		double angle = 360/60 * c.get(Calendar.SECOND);
		g2.setStroke(normalStroke);
		g2.setColor(red);
		drawRadius(g2, wx, wy, r*0.1, r, angle);
		
		angle = 360/60 * (c.get(Calendar.MINUTE) + (float)c.get(Calendar.SECOND)/60f);
		g2.setColor(Color.BLACK);
		drawRadius(g2, wx, wy, r*0.1, r*0.7, angle);
		
		angle = 360/12 * (c.get(Calendar.HOUR) + (float)c.get(Calendar.MINUTE)/60f);
		g2.setStroke(hoursStroke);
		drawRadius(g2, wx, wy, r*0.1, r*0.5, angle);
	}
	
	private void drawRadius(Graphics g, int x, int y, double rStart, double rEnd, double angle) {
		angle -=90 ;
		int x1 = x + (int)(rStart * Math.cos(angle * Math.PI/180));
		int y1 = y + (int)(rStart * Math.sin(angle * Math.PI/180));
		
		int x2 = x + (int)(rEnd * Math.cos(angle * Math.PI/180));
		int y2 = y + (int)(rEnd * Math.sin(angle * Math.PI/180));
		
		g.drawLine(x1, y1, x2, y2);
	}
	
	@Override
	public void run() {
		while (true) {
			invalidate();
			repaint();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			    break;
			}
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(200, 200);
	}
	
	public synchronized TimeZone getTimeZone() {
		return timeZone;
	}
	
	public synchronized void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
		invalidate();
		repaint();
	}
}
