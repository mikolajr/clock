package org.rydzewski.clock;

public interface Mediator {
	
	void doAbout();
	Face getFacePanel();
	void doClose();

}
