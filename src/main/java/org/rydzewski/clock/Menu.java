package org.rydzewski.clock;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.TimeZone;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;


@SuppressWarnings("serial")
public class Menu extends JMenuBar {
	private HashMap<String, TimeZone> allTimezones = new HashMap<String, TimeZone>();
	private TimeZoneChanged timeZoneListener = new TimeZoneChanged();
	private ButtonGroup timezones = new ButtonGroup();
	private final Mediator mediator;

	public Menu(Mediator m) {
		this.mediator = m;
		JMenu subMenu = new JMenu("File");
		add(subMenu);
			JMenuItem item = new JMenuItem("Exit");
			subMenu.add(item);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					mediator.doClose();
				}
			});
		subMenu = new JMenu("Timezone");
		add(subMenu);
			TimeZone here = TimeZone.getDefault();
			for(String tz : TimeZone.getAvailableIDs()) {
				TimeZone tmp = TimeZone.getTimeZone(tz);
				
				String key = tmp.getDisplayName(true, TimeZone.SHORT);
				String label = tmp.getDisplayName(true, TimeZone.LONG);
				
				if (!allTimezones.containsKey(key)) {
					allTimezones.put(key, tmp);
					
					item = new JRadioButtonMenuItem(label);
					item.setActionCommand(key);
					item.addActionListener(timeZoneListener);
					
					subMenu.add(item);
					timezones.add(item);
					if (here.equals(tmp)) timezones.setSelected(item.getModel(), true);
				}
			}
			

		subMenu = new JMenu("Help");
		add(subMenu);
			item = new JMenuItem("org.rydzewski.clock.About");
			subMenu.add(item);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					mediator.doAbout();
				}
			});
		
	}
	
	class TimeZoneChanged implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JMenuItem menu = (JMenuItem)e.getSource();
			String key = menu.getActionCommand();
			
			timezones.setSelected(menu.getModel(), true);
			TimeZone tz = allTimezones.get(key);
			mediator.getFacePanel().setTimeZone(tz);
		}
	}

	

}
