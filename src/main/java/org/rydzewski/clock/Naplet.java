package org.rydzewski.clock;

import javax.swing.JApplet;


@SuppressWarnings("serial")
public class Naplet extends JApplet implements Mediator {

	private Face face;

	@Override
	public void init() {
		super.init();
		face = new Face();
		getContentPane().add(face);
		setJMenuBar(new Menu(this));
	}

	@Override
	public void doAbout() {
		new About(null).setVisible(true);
	}

	@Override
	public void doClose() {
	}

	@Override
	public Face getFacePanel() {
		return face;
	}

}
