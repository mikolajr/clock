package org.rydzewski.clock;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.SwingUtilities;


@SuppressWarnings("serial")
public class WorldClock extends JFrame implements Mediator {

	private Face face;
	
	public WorldClock() {
		super("World clock");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		face = new Face();
		add(face);

		JMenuBar menu = new Menu(this);
		setJMenuBar(menu);
		
		setBackground(Color.white);
		
		pack();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new WorldClock().setVisible(true);
			}
		});
	}

	@Override
	public void doAbout() {
		new About(this).setVisible(true);
	}

	@Override
	public void doClose() {
		dispose();
	}

	@Override
	public Face getFacePanel() {
		return face;
	}
	
	
}
